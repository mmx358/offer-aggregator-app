package com.teddyguitarhero.offeraggregatorapp.model;

import lombok.Value;
import org.solidbank.client.model.ApplicationRequest;

import java.math.BigDecimal;

@SuppressWarnings("ClassCanBeRecord")
@Value public class CommonApplicationRequest {

    String phone;
    String email;
    BigDecimal monthlyIncome;
    BigDecimal monthlyExpenses;
    BigDecimal monthlyCreditLiabilities;
    ApplicationRequest.MaritalStatusEnum maritalStatus;
    Integer dependents;
    Boolean agreeToDataSharing;
    Boolean agreeToBeScored;
    BigDecimal amount;
}
