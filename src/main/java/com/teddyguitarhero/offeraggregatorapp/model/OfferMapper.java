package com.teddyguitarhero.offeraggregatorapp.model;

import org.mapstruct.Mapper;

import java.util.UUID;

@Mapper(componentModel = "spring")
public interface OfferMapper {

    CommonApplication toApplication(org.fastbank.client.model.Application application);

    CommonApplication toApplication(org.solidbank.client.model.Application application);

    default UUID map(String value) {
        return UUID.fromString(value);
    }
}
