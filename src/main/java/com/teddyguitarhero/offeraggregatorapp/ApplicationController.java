package com.teddyguitarhero.offeraggregatorapp;

import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;
import com.teddyguitarhero.offeraggregatorapp.model.OfferResult;
import com.teddyguitarhero.offeraggregatorapp.service.OfferAggregationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/application")
@RequiredArgsConstructor
public class ApplicationController {

    private final OfferAggregationService offerAggregationService;

    @PostMapping
    public List<OfferResult> postAndWaitForOffers(@RequestBody CommonApplicationRequest applicationRequest) {
        return offerAggregationService.applyForOffers(applicationRequest);
    }
}
