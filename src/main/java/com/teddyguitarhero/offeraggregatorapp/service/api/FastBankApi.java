package com.teddyguitarhero.offeraggregatorapp.service.api;

import org.fastbank.ApiClient;
import org.fastbank.api.DefaultApi;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FastBankApi extends DefaultApi {

    public FastBankApi(RestTemplate bankApiRestTemplate) {
        super(new ApiClient(bankApiRestTemplate));
    }
}
