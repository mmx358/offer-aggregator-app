package com.teddyguitarhero.offeraggregatorapp.service.api;

import com.teddyguitarhero.offeraggregatorapp.model.ApplicationRequestMapper;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplication;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;
import com.teddyguitarhero.offeraggregatorapp.model.OfferMapper;
import com.teddyguitarhero.offeraggregatorapp.service.OfferProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FastBankService implements OfferProvider {

    private final FastBankApi api;
    private final ApplicationRequestMapper applicationRequestMapper;
    private final OfferMapper offerMapper;

    @Override
    public String getName() {
        return "Fast Bank";
    }

    @Override
    public CommonApplication submitApplication(CommonApplicationRequest application) {
        return Optional.of(application)
                .map(applicationRequestMapper::toFastBankApplicationRequest)
                .map(api::addApplication)
                .map(offerMapper::toApplication)
                .orElse(null);
    }

    @Override
    public CommonApplication retrieveApplication(UUID id) {
        return offerMapper.toApplication(api.getApplicationById(id));
    }
}
