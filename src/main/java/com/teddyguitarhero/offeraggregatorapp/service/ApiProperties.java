package com.teddyguitarhero.offeraggregatorapp.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@ConfigurationProperties(prefix = "api.retry")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiProperties {

    private Duration backoffInterval;
    private Duration timeout;
}
