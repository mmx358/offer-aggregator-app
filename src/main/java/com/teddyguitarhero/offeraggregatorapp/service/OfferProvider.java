package com.teddyguitarhero.offeraggregatorapp.service;

import com.teddyguitarhero.offeraggregatorapp.model.CommonApplication;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;

import java.util.UUID;

public interface OfferProvider {

    String getName();

    CommonApplication submitApplication(CommonApplicationRequest application);

    CommonApplication retrieveApplication(UUID id);
}
