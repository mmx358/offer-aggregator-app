package com.teddyguitarhero.offeraggregatorapp.service;

import com.teddyguitarhero.offeraggregatorapp.model.CommonApplication;
import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;
import com.teddyguitarhero.offeraggregatorapp.model.CommonOffer;
import com.teddyguitarhero.offeraggregatorapp.model.OfferResult;
import com.teddyguitarhero.offeraggregatorapp.service.api.FastBankService;
import com.teddyguitarhero.offeraggregatorapp.service.api.SolidBankService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.teddyguitarhero.offeraggregatorapp.model.OfferResult.OfferResultStatus.*;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Slf4j
@RequiredArgsConstructor
public class OfferAggregationService {

    private final FastBankService fastBankService;
    private final SolidBankService solidBankService;
    private final ApiProperties apiProperties;
    private final RetryTemplate retryTemplate;

    public List<OfferResult> applyForOffers(CommonApplicationRequest application) {
        List<CompletableFuture<OfferResult>> completableFutures = Stream.of(fastBankService, solidBankService)
                .map(toCompletableFuture(application))
                .collect(Collectors.toList());

        return completableFutures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    private Function<OfferProvider, CompletableFuture<OfferResult>> toCompletableFuture(CommonApplicationRequest applicationRequest) {
        return service -> CompletableFuture.supplyAsync(
                () -> {
                    CommonApplication application = submitAndWaitForOffer(applicationRequest, service);
                    return mapToOfferResult(service.getName(), application.getOffer());
                })
                .completeOnTimeout(
                        timedOutOfferResult(service.getName()),
                        apiProperties.getTimeout().toMillis(),
                        MILLISECONDS
                )
                .exceptionally(mapExceptionToOfferResult(service.getName()));
    }

    private CommonApplication submitAndWaitForOffer(
            CommonApplicationRequest applicationRequest,
            OfferProvider provider
    ) {
        CommonApplication application = submitAndGetResponse(applicationRequest, provider);

        if (application.isProcessed()) {
            return application;
        } else {
            try {
                MILLISECONDS.sleep(apiProperties.getBackoffInterval().toMillis());
            } catch (InterruptedException ex) {
                throw new OfferError(provider.getName(), UNEXPECTED_ERROR, ex);
            }
            return waitForOffer(provider, application.getId());
        }
    }

    private CommonApplication submitAndGetResponse(
            CommonApplicationRequest applicationRequest,
            OfferProvider provider
    ) {
        try {
            log.debug("Calling \"" + provider.getName() + "\" provider to submit application");
            return provider.submitApplication(applicationRequest);
        } catch (RestClientResponseException ex) {
            if (statusesOf(BAD_REQUEST).contains(ex.getRawStatusCode())) {
                throw new OfferError(provider.getName(), API_ERROR, ex);
            } else {
                throw new OfferError(provider.getName(), UNEXPECTED_ERROR, ex);
            }
        } catch (Throwable throwable) {
            throw new OfferError(provider.getName(), UNEXPECTED_ERROR, throwable);
        }
    }

    private CommonApplication waitForOffer(OfferProvider provider, UUID applicationId) {
        try {
            return retryTemplate.execute(
                    context -> {
                        log.debug("Calling \"" + provider.getName() + "\" provider to retrieve application with id=" + applicationId);
                        CommonApplication application = provider.retrieveApplication(applicationId);

                        if (application.isProcessed()) {
                            log.trace("Application with id=" + applicationId + " is processed");
                            return application;
                        } else {
                            log.trace("Application with id=" + applicationId + " is draft");
                            throw new ApplicationStillProcessingException();
                        }
                    });
        } catch (RestClientResponseException ex) {
            if (statusesOf(BAD_REQUEST, NOT_FOUND).contains(ex.getRawStatusCode())) {
                throw new OfferError(provider.getName(), API_ERROR, ex);
            } else {
                throw new OfferError(provider.getName(), UNEXPECTED_ERROR, ex);
            }
        } catch (Throwable throwable) {
            throw new OfferError(provider.getName(), UNEXPECTED_ERROR, throwable);
        }
    }

    private List<Integer> statusesOf(HttpStatus... statuses) {
        return Stream.of(statuses)
                .map(HttpStatus::value)
                .collect(Collectors.toList());
    }

    private OfferResult mapToOfferResult(String providerName, CommonOffer offer) {
        return Optional.ofNullable(offer)
                .map(it -> new OfferResult(
                        providerName,
                        OFFERED,
                        Optional.of(it)
                ))
                .orElse(new OfferResult(
                        providerName,
                        DECLINED,
                        Optional.empty()
                ));
    }

    private OfferResult timedOutOfferResult(String providerName) {
        return new OfferResult(
                providerName,
                TIMED_OUT,
                Optional.empty()
        );
    }

    private Function<Throwable, OfferResult> mapExceptionToOfferResult(String providerName) {
        return exception -> {
            Throwable cause = exception.getCause();

            log.error("Exception was thrown", cause);

            if (cause instanceof OfferError) {
                return new OfferResult(
                        ((OfferError) cause).getProviderName(),
                        ((OfferError) cause).getStatus(),
                        Optional.empty()
                );
            } else {
                return new OfferResult(
                        providerName,
                        UNEXPECTED_ERROR,
                        Optional.empty()
                );
            }
        };
    }
}
