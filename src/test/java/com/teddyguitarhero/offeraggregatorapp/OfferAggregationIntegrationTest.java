package com.teddyguitarhero.offeraggregatorapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.teddyguitarhero.offeraggregatorapp.service.ApiProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.ResponseCreator;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplication.*;
import static com.teddyguitarhero.offeraggregatorapp.test.samples.SampleApplicationRequest.sampleApplicationRequest;
import static com.teddyguitarhero.offeraggregatorapp.test.service.AwaitUntilExecutionCompletes.awaitAtMost;
import static com.teddyguitarhero.offeraggregatorapp.test.service.MultipleResponseCreator.withResponses;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.ExpectedCount.times;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = {
        "api.retry.backoffInterval=1s",
        "api.retry.timeout=10s"
})
@AutoConfigureMockMvc
public class OfferAggregationIntegrationTest {

    private Duration longestApiCall;

    @Autowired
    private RestTemplate bankApiRestTemplate;

    private MockRestServiceServer server;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ApiProperties apiProperties;

    @BeforeEach
    void setUp() {
        longestApiCall = apiProperties.getTimeout().plus(apiProperties.getBackoffInterval().multipliedBy(2));

        server = MockRestServiceServer
                .bindTo(bankApiRestTemplate)
                .ignoreExpectOrder(true)
                .build();
    }

    @Test
    @DisplayName("should submit application request and return errors when APIs responded with errors")
    void shouldSubmitApplicationAndReturnErrorsWhenApiRespondedWithErrors() throws Exception {
        server.expect(once(), requestTo("https://shop.stage.klix.app:443/api/FastBank/applications"))
                .andExpect(method(POST))
                .andRespond(withBadRequest());

        server.expect(once(), requestTo("https://shop.stage.klix.app:443/api/SolidBank/applications"))
                .andExpect(method(POST))
                .andRespond(withSuccessContaining(draftOffer()));

        server.expect(times(5), requestTo(startsWith("https://shop.stage.klix.app:443/api/SolidBank/applications/")))
                .andExpect(method(GET))
                .andRespond(withResponses(
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withStatus(INTERNAL_SERVER_ERROR)
                ));

        String responseBody = awaitAtMost(longestApiCall)
                .toComplete(() -> mockMvc.perform(
                        post("/application")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                                        sampleApplicationRequest()))
                )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString());

        server.verify();

        JsonNode offerResults = objectMapper.readTree(responseBody);

        JsonNode expectedOfferResults = objectMapper.readTree(
                """
                        [
                            {
                               "providerName":"Fast Bank",
                               "status":"API_ERROR",
                               "offer":null
                            },
                            {
                               "providerName":"Solid Bank",
                               "status":"UNEXPECTED_ERROR",
                               "offer":null
                            }
                         ]
                         """);

        assertThat(offerResults).isEqualTo(expectedOfferResults);
    }

    @Test
    @DisplayName("should submit application request and return offers when APIs responded with offers")
    void shouldSubmitApplicationAndReturnOffersWhenApiRespondedWithOfferOnSubmit() throws Exception {
        server.expect(once(), requestTo("https://shop.stage.klix.app:443/api/FastBank/applications"))
                .andExpect(method(POST))
                .andRespond(withSuccessContaining(declinedOffer()));

        server.expect(once(), requestTo("https://shop.stage.klix.app:443/api/SolidBank/applications"))
                .andExpect(method(POST))
                .andRespond(withSuccessContaining(draftOffer()));

        server.expect(times(5), requestTo(startsWith("https://shop.stage.klix.app:443/api/SolidBank/applications/")))
                .andExpect(method(GET))
                .andRespond(withResponses(
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(draftOffer()),
                        withSuccessContaining(approvedOffer())
                ));

        String responseBody = awaitAtMost(longestApiCall)
                .toComplete(() -> mockMvc.perform(
                        post("/application")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                                        sampleApplicationRequest()))
                )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString());

        server.verify();

        JsonNode offerResults = objectMapper.readTree(responseBody);

        JsonNode expectedOfferResults = objectMapper.readTree(
                """
                        [
                            {
                               "providerName":"Fast Bank",
                               "status":"DECLINED",
                               "offer":null
                            },
                            {
                               "providerName":"Solid Bank",
                               "status":"OFFERED",
                               "offer":{
                                  "monthlyPaymentAmount":1716.67,
                                  "totalRepaymentAmount":5150.00,
                                  "numberOfPayments":3,
                                  "annualPercentageRate":10.11,
                                  "firstRepaymentDate":"2021-08-27"
                               }
                            }
                         ]
                         """);

        assertThat(offerResults).isEqualTo(expectedOfferResults);
    }

    private ResponseCreator withSuccessContaining(Object object) throws JsonProcessingException {
        return withSuccess(objectMapper.writeValueAsString(object), MediaType.APPLICATION_JSON);
    }
}
