package com.teddyguitarhero.offeraggregatorapp.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;


class OfferMapperTest {

    private final OfferMapper mapper = new OfferMapperImpl();

    @Test
    void shouldMapFastBankApplicationToCommonApplication() {
        org.fastbank.client.model.Application original = fastBankApplication();

        CommonApplication actual = mapper.toApplication(original);

        assertThat(actual.getId().toString()).isEqualTo(original.getId());
        assertThat(actual.getStatus().name()).isEqualTo(requireNonNull(original.getStatus()).name());
        assertThat(actual.getOffer()).usingRecursiveComparison()
                .isEqualTo(original.getOffer());
    }

    @Test
    void shouldMapSolidBankApplicationToCommonApplication() {
        org.solidbank.client.model.Application original = solidBankApplication();

        CommonApplication actual = mapper.toApplication(original);

        assertThat(actual.getId().toString()).isEqualTo(original.getId());
        assertThat(actual.getStatus().name()).isEqualTo(requireNonNull(original.getStatus()).name());
        assertThat(actual.getOffer()).usingRecursiveComparison()
                .isEqualTo(original.getOffer());
    }

    private org.fastbank.client.model.Application fastBankApplication() {
        org.fastbank.client.model.Application application = new org.fastbank.client.model.Application();
        application.setId(UUID.randomUUID().toString());
        application.setStatus(org.fastbank.client.model.Application.StatusEnum.PROCESSED);
        application.setOffer(new org.fastbank.client.model.Offer());
        return application;
    }

    private org.solidbank.client.model.Application solidBankApplication() {
        org.solidbank.client.model.Application application = new org.solidbank.client.model.Application();
        application.setId(UUID.randomUUID().toString());
        application.setStatus(org.solidbank.client.model.Application.StatusEnum.PROCESSED);
        application.setOffer(new org.solidbank.client.model.Offer());
        return application;
    }
}
