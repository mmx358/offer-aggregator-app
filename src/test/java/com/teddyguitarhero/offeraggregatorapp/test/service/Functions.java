package com.teddyguitarhero.offeraggregatorapp.test.service;

import org.awaitility.Awaitility;
import org.awaitility.core.ConditionFactory;

import java.util.concurrent.Callable;
import java.util.function.Predicate;

public class Functions {

    /**
     * {@link Predicate} which always evaluates to {@code true}.
     * <br>
     * Use to stop {@link Awaitility#await()} and return the value from {@link ConditionFactory#until(Callable, Predicate)}
     * right after supplier block finishes.
     */
    public static Predicate<Object> executionCompletes() {
        return any -> true;
    }
}
