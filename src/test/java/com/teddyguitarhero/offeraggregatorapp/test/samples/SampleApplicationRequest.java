package com.teddyguitarhero.offeraggregatorapp.test.samples;

import com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationRequest;

import java.math.BigDecimal;

import static org.solidbank.client.model.ApplicationRequest.MaritalStatusEnum.MARRIED;

public class SampleApplicationRequest {

    public static CommonApplicationRequest sampleApplicationRequest() {
        return new CommonApplicationRequest(
                "+37166554433",
                "mister.johnson@email.com",
                new BigDecimal("350.39"),
                new BigDecimal("279.47"),
                new BigDecimal("23.98"),
                MARRIED,
                2,
                true,
                true,
                new BigDecimal("3090.00")
        );
    }
}
