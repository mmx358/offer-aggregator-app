package com.teddyguitarhero.offeraggregatorapp.test.samples;

import com.teddyguitarhero.offeraggregatorapp.model.CommonApplication;
import com.teddyguitarhero.offeraggregatorapp.model.CommonOffer;

import java.math.BigDecimal;
import java.util.UUID;

import static com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationStatus.DRAFT;
import static com.teddyguitarhero.offeraggregatorapp.model.CommonApplicationStatus.PROCESSED;

public class SampleApplication {

    public static CommonApplication approvedOffer() {
        return new CommonApplication(
                UUID.randomUUID(),
                PROCESSED,
                new CommonOffer(
                        new BigDecimal("1716.67"),
                        new BigDecimal("5150.00"),
                        3,
                        new BigDecimal("10.11"),
                        "2021-08-27"
                )
        );
    }

    public static CommonApplication draftOffer() {
        return new CommonApplication(UUID.randomUUID(), DRAFT, null);
    }

    public static CommonApplication declinedOffer() {
        return new CommonApplication(UUID.randomUUID(), PROCESSED, null);
    }
}
