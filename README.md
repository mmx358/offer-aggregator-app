# Description

This project is a proof of concept of an aggregator service. It accepts an application (submission form) for loan offer
object, maps it to service-specific request objects and submits them to several services. If a decision about offering
or declining the application is not found in the response, then it polls the service until the decision is received or
polling times out.

## Technologies used

- Java 16
- Spring Boot 2.5.2
- OpenAPI Generator

... and other less significant stuff.

# Leftovers

Not all ideas are implemented due to very limited time, so I also post my list of things I planned to do next (or could
do if it was a real service). So you know I didn't forget about them.

### The list:

- [ ] Use fixed thread pool for `CompletableFuture`-s not to grow memory usage (although only relevant for production with high load)
- [ ] Check mock interaction in `OfferAggregationServiceTest`
- [ ] Introduce validation of `CommonApplicationRequest` (probably will need some cross-field validations)
- [ ] Change `firstRepaymentDate` in `CommonOffer` to `LocalDate`, because any date format could be mapped to the
  unified one
- [ ] Change `CommonApplication#id` to `String`, because potentially implementation can change
- [ ] Differentiate between timeout when always received `DRAFT` status, and the one which happens, for example, because
  request to bank API times out
- [ ] Calculate correct expected time for timeouts in tests to test operation execution time
- [ ] Use `RestTemplateBuilder` and `@RestClientTest`
- [ ] Allow filtering Offer result objects by status (e.g., show only successfully completed with offer)
- [ ] An endpoint more suitable for frontend (with "built-in" filtering by status and returning an error response if
  nothing to show, etc.)
- [ ] Create a demo frontend client
- [ ] Check possibility to configure OpenAPI generation differently, to use single Gradle subproject or even integrate
  generated clients into the main project instead of having one subproject per generated client
- [ ] Write client tests for `FastBankService` and `SolidBankService`
- [ ] "Failsafe" library or any other alternative instead of Spring Retry, because it looks like an overkill for the
  task

# Setup and Run

## Prerequisites

- Java 16

## How to start

Windows shell (cmd): go to project directory and run `gradlew bootRun`

Bash: go to project directory and execute `./gradlew bootRun`

Or use whatever IDE.

## How to try

### Swagger UI

1. Go to `http://localhost:8080/swagger-ui.html`
   or `http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config`

2.
    - See the lonely `POST /application` endpoint. Try it and use JSON object like the one at the end of this file.
    - Or via HTTP client by calling `POST http://localhost:8080/application` with the JSON object (like one below) in the
    body and `application/json` as a content type.

### Sample application JSON

    {
      "phone": "+37166554433",
      "email": "mister.johnson@email.com",
      "monthlyIncome": 350.39,
      "monthlyExpenses": 279.47,
      "monthlyCreditLiabilities": 23.98,
      "maritalStatus": "MARRIED",
      "dependents": 2,
      "agreeToDataSharing": true,
      "agreeToBeScored": true,
      "amount": 3090.00
    }
